console.log("hi");

let studNumA = "2020-1923";
let studNumB = "2020-1924";
let studNumC = "2020-1925";
let studNumD = "2020-1926";
let studNumE = "2020-1927";

console.log(studNumA);
console.log(studNumB);
console.log(studNumC);
console.log(studNumD);
console.log(studNumE);

let studID = ["2020-1923","2020-1924","2020-1925","2020-1926","2020-1927"];

console.log(studID);

/*
	Arrays
		- used to store multiple related values in a single variable
		- declared using the square bracket [] also known as "Array Literals"

		Syntax:
			let/const arrayName = [elementA,elementB,...,elementC];
*/

let grades = [96.8,85.7,90.3,91.5];
let compBrand = ["Acer","Lenovo","HP","Samsung","Asus","Toshiba"];

console.log(grades);
console.log(compBrand);

let mixedArray = [12,"Asus",null,undefined,{}];
console.log(mixedArray);

let myTasks = [
			"drink html",
			"eat js",
			"inhales css",
			"bake sass"
			];

console.log(myTasks);

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";
let city4 = "Beijing";

let cities = [city1,city2,city3,city4];
let citiesSample = ["Tokyo","Manila","New York","Beijing"];

// Objects are declared using curly braces ({})

console.log(cities);
/*console.log(citiesSample);
console.log(cities = citiesSample);
*/

// Array length property
/*
	- to set or get the items or elements in an array 
*/

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

let fullName = "Fidel Ramos";
console.log(fullName.length);

myTasks.length--
console.log(myTasks.length);
console.log(myTasks);

let theBeatles = ["John","Paul","Ringo","George"];
console.log(theBeatles.length);

theBeatles.length++;
console.log(theBeatles.length);

console.log(theBeatles);

theBeatles[4] = "Rey";
console.log(theBeatles);

/*
	Accessing elements of an Array

		Syntax:
			arrayName[index];
*/

console.log(grades[0]);
console.log(compBrand[3]);

let lakersLegend = ["KObe","Shaq","Magic","Kareem"];
console.log(lakersLegend[0]);
console.log(lakersLegend[2]);

let currentLaker = lakersLegend[2];

console.log(currentLaker);

console.log("Array before reassignment");
console.log(lakersLegend);
lakersLegend[2] = "Pau Gasol";
console.log("Array after reassignment");
console.log(lakersLegend);

let bullsLegend = ["Jordan","Pippen","Rodman","Rose","Kukoc"];

let lastelementIndex = bullsLegend.length - 1;
console.log(bullsLegend.length);
console.log(bullsLegend[lastelementIndex]);


//Adding Items into an Array
let newArr = [];
console.log(newArr);

console.log(newArr[0]);
newArr[0] = "Jenny";
console.log(newArr);
newArr[1] = "Jisoo";
console.log(newArr);

newArr[newArr.length++] = "Lisa";
console.log(newArr);

let superHeroes = ["Iron Man","SpiderMan","Captain Ameria"];
function myFunction(arg){
	console.log(superHeroes.length);
	superHeroes[superHeroes.length++] = arg;
	console.log(superHeroes);

};

myFunction("Hulk");

console.log(superHeroes);

function displayHero(hero){
	let thisHero = superHeroes[hero];
	return thisHero;
};

console.log(displayHero(2));

for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5,12,24,56,77,88,97];

for(let index = 0; index < numArr.length; index++){
	if (numArr[index] % 5 === 0){
		console.log(numArr[index] + " is dividible by 5");
	}else{
		console.log(numArr[index] + " is not divisible by 5"); 
	}
}

//Multidimensional Arrays
/*
	Arrays within an array
*/

let chessBoard = [
	["a1","b1","c1","d1","e1","f1","g1","h1"],
	["a2","b2","c2","d2","e2","f2","g2","h2"],
	["a3","b3","c3","d3","e3","f3","g3","h3"],
	["a4","b4","c4","d4","e4","f4","g4","h4"],
	["a5","b5","c5","d5","e5","f5","g5","h5"],
	["a6","b6","c6","d6","e6","f6","g6","h6"],
	["a7","b7","c7","d7","e7","f7","g7","h7"],
	["a8","b8","c8","d8","e8","f8","g8","h8"]
];

console.log(chessBoard);
console.log(chessBoard[1][4]);
console.log("Pawn moves to " + chessBoard[7][4]);




